/*

(               )                                        )
( )\     )     ( /(       (                  (  (     ) ( /((
)((_) ( /(  (  )\())`  )  )(   (  `  )   (   )\))( ( /( )\())\  (   (
((_)_  )(_)) )\((_)\ /(/( (()\  )\ /(/(   )\ ((_))\ )(_)|_))((_) )\  )\ )
| _ )((_)_ ((_) |(_|(_)_\ ((_)((_|(_)_\ ((_) (()(_|(_)_| |_ (_)((_)_(_/(
| _ \/ _` / _|| / /| '_ \) '_/ _ \ '_ \/ _` |/ _` |/ _` |  _|| / _ \ ' \))
|___/\__,_\__||_\_\| .__/|_| \___/ .__/\__,_|\__, |\__,_|\__||_\___/_||_|
|_|           |_|         |___/

For more information on back-propagation, refer to:
Chapter 18 of Russel and Norvig (2010).
Artificial Intelligence - A Modern Approach.
*/

#include "CNeuralNet.h"
#include "utils.h"

/**
The constructor of the neural network. This constructor will allocate memory
for the weights of both input->hidden and hidden->output layers, as well as the input, hidden
and output layers.
*/
CNeuralNet::CNeuralNet(uint inputLayerSize, uint hiddenLayerSize, uint outputLayerSize, double lRate, double mse_cutoff)
	: inputLayerSize(inputLayerSize), hiddenLayerSize(hiddenLayerSize), outputLayerSize(outputLayerSize), lRate(lRate), mse_cutoff(mse_cutoff), hiddenLayerOutputs(hiddenLayerSize), outputLayerOutputs(outputLayerSize), hiddenLayerErrors(hiddenLayerSize), outputLayerErrors(outputLayerSize)
{
	hiddenLayerWeightsIn.resize(inputLayerSize);
	for (uint i = 0; i < inputLayerSize; i++)
	{
		hiddenLayerWeightsIn[i].resize(hiddenLayerSize);
	}

	hiddenLayerWeightsOut.resize(hiddenLayerSize);
	for (uint i = 0; i < hiddenLayerSize; i++)
	{
		hiddenLayerWeightsOut[i].resize(outputLayerSize);
	}

	initWeights();
}
/**
The destructor of the class. All allocated memory will be released here.
*/
CNeuralNet::~CNeuralNet()
{
	// Deliberately left empty.
}
/**
Function to initialize both layers of weights to random numbers.
*/
void CNeuralNet::initWeights()
{
	for (uint i = 0; i < inputLayerSize; i++)
	{
		for (uint j = 0; j < hiddenLayerSize; j++)
		{
			hiddenLayerWeightsIn[i][j] = RandomClamped();
		}
	}

	for (uint i = 0; i < hiddenLayerSize; i++)
	{
		for (uint j = 0; j < outputLayerSize; j++)
		{
			hiddenLayerWeightsOut[i][j] = RandomClamped();
		}
	}
}
/**
This is the forward feeding part of back propagation.
1. This should take the input and copy the memory (use memcpy / std::copy)
to the allocated _input array.
2. Compute the output at the hidden layer nodes. Assume the network is completely connected.
3. Repeat step 2, but this time compute the output at the output layer.
*/
void CNeuralNet::feedForward(std::vector<double> inputs)
{
	// Calculate the hidden layer outputs.
	hiddenLayerOutputs.clear();
	for (uint i = 0; i < hiddenLayerSize; i++)
	{
		double output = 0.0;
		for (uint j = 0; j < inputLayerSize; j++)
		{
			output += hiddenLayerWeightsIn[j][i] * inputs[j];
		}
		output = sigmoid(output);
		hiddenLayerOutputs.push_back(output);
	}

	// Calculate the output layer outputs.
	outputLayerOutputs.clear();
	for (uint i = 0; i < outputLayerSize; i++)
	{
		double output = 0.0;
		for (uint j = 0; j < hiddenLayerSize; j++)
		{
			output += hiddenLayerWeightsOut[j][i] * hiddenLayerOutputs[j];
		}
		output = sigmoid(output);
		outputLayerOutputs.push_back(output);
	}
}
/**
This is the actual back propagation part of the back propagation algorithm.
It should be executed after feeding forward. Given a vector of desired outputs,
one computes the error at the hidden and output layers (allocate some memory for this) and
assign 'blame' for any error to all the nodes that fed into the current node, based on the
weight of the connection.
Steps:
1. Compute the errors at the output layer.
2. Compute the errors at the hidden layer.
3. Adjust the weights from the hidden to the output layer.
4. Adjust the weights from the input to the hidden layer.
5. REMEMBER TO FREE ANY ALLOCATED MEMORY WHEN DONE (or use std::vector).
*/
void CNeuralNet::propagateErrorBackward(std::vector<double> desiredOutput, std::vector<double> inputs)
{
	// Calculate the output layer errors.
	for (uint i = 0; i < outputLayerSize; i++)
	{
		double error = outputLayerOutputs[i] * ((1.0 - (outputLayerOutputs[i])) * (desiredOutput[i] - (outputLayerOutputs[i])));
		outputLayerErrors[i] = error;
	}

	// Calculate the hidden layer errors.
	for (uint i = 0; i < hiddenLayerSize; i++)
	{
		double error = 0.0;
		for (uint j = 0; j < outputLayerSize; j++)
		{
			error += (hiddenLayerWeightsOut[i][j]) * outputLayerErrors[j];
		}
		error *= (hiddenLayerOutputs[i]) * (1.0 - (hiddenLayerOutputs[i]));
		hiddenLayerErrors[i] = error;
	}

	// Update the weights between the hidden layer and output layer.
	for (uint i = 0; i < hiddenLayerSize; i++)
	{
		for (uint j = 0; j < outputLayerSize; j++)
		{
			double delta = lRate * outputLayerErrors[j] * hiddenLayerOutputs[i];
			hiddenLayerWeightsOut[i][j] += delta;
		}
	}

	// Update the weights between the input layer and hidden layer.
	for (uint i = 0; i < inputLayerSize; i++)
	{
		for (uint j = 0; j < hiddenLayerSize; j++)
		{
			double delta = lRate * hiddenLayerErrors[j] * inputs[i];
			hiddenLayerWeightsIn[i][j] += delta;
		}
	}
}
/**
This computes the mean squared error.
A very handy formula to test numeric output with.
*/
double CNeuralNet::meanSquaredError(std::vector<double> desiredOutput)
{
	double sum = 0.0;
	for (uint i = 0; i < outputLayerSize; i++)
	{
		double error = desiredOutput[i] - getOutput(i);
		sum += (error * error);
	}
	return sum / outputLayerSize;
}
/**
This trains the neural network according to the back propagation algorithm.
The primary steps are:
For each training pattern:
Feed forward.
Propagate backward.
Until the MSE becomes suitably small.
*/
void CNeuralNet::train(std::vector<std::vector<double> > inputs, std::vector<std::vector<double> > outputs, uint trainingSetSize)
{
	double mse = DBL_MAX;
	while (mse >= mse_cutoff)
	{
		for (uint i = 0; i < trainingSetSize; i++)
		{
			feedForward(inputs[i]);
			propagateErrorBackward(outputs[i], inputs[i]);
			mse = meanSquaredError(outputs[i]);
			if (mse <= mse_cutoff)
			{
				break;
			}
		}
	}
}
/**
Once the network is trained, one can simply feed it some input through the feed forward
function and take the maximum value as the classification.
*/
uint CNeuralNet::classify(const std::vector<double> input)
{
	feedForward(input);
	if (getOutput(0) < getOutput(1))
	{
		return 1;
	}
	return 0;
}
/**
Get the output at the specified index.
*/
double CNeuralNet::getOutput(uint index) const
{
	return outputLayerOutputs[index];
}
/**
Sigmoid activation function.
*/
double CNeuralNet::sigmoid(double x)
{
	double exponentValue;
	double returnValue;

	// Exponent calculation.
	exponentValue = exp((double)-x);

	// Final value from sigmoid function.
	returnValue = 1.0 / (1.0 + exponentValue);

	return returnValue;
}
