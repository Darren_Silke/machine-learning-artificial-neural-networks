/*
 * CNeuralNet.h
 *
 *  Created on: 05 October 2015
 *      Author: Darren Silke
 */

#ifndef CNEURALNET_H_
#define CNEURALNET_H_
#include <vector>
#include <cmath>
#include <algorithm>
#include <stdlib.h>
#include <cstring>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef unsigned int uint;

class CNeuralNet 
{
public:
	CNeuralNet(uint inputLayerSize, uint hiddenLayerSize, uint outputLayerSize, double lRate, double mse_cutoff);
	void initWeights();
	void train(std::vector<std::vector<double> > inputs, std::vector<std::vector<double> > outputs, uint trainingSetSize);
	uint classify(const std::vector<double> input);
	double getOutput(uint index) const;
	virtual ~CNeuralNet();
protected:
	void feedForward(std::vector<double> inputs);
	void propagateErrorBackward(std::vector<double> desiredOutput, std::vector<double> inputs);
	double meanSquaredError(std::vector<double> desiredOutput);
private:
	double sigmoid(double x);

	uint inputLayerSize;
	uint hiddenLayerSize;
	uint outputLayerSize;
	double lRate;
	double mse_cutoff;
	std::vector<std::vector<double> > hiddenLayerWeightsIn;
	std::vector<std::vector<double> > hiddenLayerWeightsOut;
	std::vector<double> hiddenLayerOutputs;
	std::vector<double> outputLayerOutputs;
	std::vector<double> hiddenLayerErrors;
	std::vector<double> outputLayerErrors;
};
#endif /* CNEURALNET_H_ */
